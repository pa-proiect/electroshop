#include "InputTextBox.h"

InputTextBox::InputTextBox()
{
    m_input = "";
}

void InputTextBox::SetType(const std::string& type)
{
    m_type = ParseStringToInputType(type);
}

void InputTextBox::SetOrigin(const sf::Vector2f& size)
{
    m_textBox.setOrigin(size);
    m_textBox.setOutlineColor(sf::Color(36, 33, 48));
    m_textBox.setOutlineThickness(1);
    m_textBox.setSize(sf::Vector2f(300, 40));
}

void InputTextBox::SetTextOrigin(const sf::Vector2f& origin)
{
    m_text.setOrigin(origin);
}

void InputTextBox::SetTextFont(const sf::Font& font)
{
    m_text.setFont(font);
    m_text.setFillColor(sf::Color::Black);
    m_text.setCharacterSize(18);
}

void InputTextBox::SetInput(const sf::String& input)
{
    m_input += input;
    m_text.setString(m_input);
}

const sf::RectangleShape& InputTextBox::GetTextBox() const
{
    return m_textBox;
}

const sf::Text& InputTextBox::GetText() const
{
    return m_text;
}

const InputType& InputTextBox::GetInputType() const
{
    return m_type;
}