#include "WishList.h"
#include <iostream>

WishList::WishList()
{
}

void WishList::AddProductImages()
{

}

void WishList::AddProduct(Product newProduct)
{
	m_wishProducts.push_front(newProduct);
}

void WishList::RemoveProduct(Product product)
{
	m_wishProducts.remove(product);
}

void WishList::AddToShoppingCart(Product product)
{
	m_products.AddProduct(product);
}

void WishList::MoveElements(const std::string& direction)
{
	if (direction == "up")
	{
		for (int index = 0; index < m_productImages.size(); index++)
		{
			m_productRectangles.at(index).move(m_productRectangles.at(index).getPosition().x, 20);
			m_productSprite.at(index).move(m_productSprite.at(index).getPosition().x, 20);
		}
	}
	if (direction == "down")
	{
		for (int index = 0; index < m_productImages.size(); index++)
		{
			m_productRectangles.at(index).move(m_productRectangles.at(index).getPosition().x, -20);
			m_productSprite.at(index).move(m_productSprite.at(index).getPosition().x, -20);
		}
	}
	if (direction != "up" && direction != "down")
		std::cerr << "Invalid direction" << std::endl;
}

void WishList::CreateFrame()
{
	m_wishFrame.create(sf::VideoMode(600, 640), "Electro Shop");

	SetMenuBar();

	InitializeFonts();

	SetLogo();
}

void WishList::SetMenuBar()
{
	m_menuBar.setSize(sf::Vector2f(640, 70));
	m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void WishList::InitializeFonts()
{
	if (!m_logoFont.loadFromFile("LogoFont.ttf"))
		std::cerr << "LogoFont can't be loaded!" << std::endl;

	if (!m_buttonFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void WishList::SetLogo()
{
	m_logo.setString("ElectroShop");
	m_logo.setFont(m_logoFont);
	m_logo.setCharacterSize(45);
	m_logo.setFillColor(sf::Color(255, 206, 43));
	m_logo.setOrigin(sf::Vector2f(-190, 0));
}

void WishList::SetBuyButton()
{
	m_buyButton.setSize(sf::Vector2f(200, 20));
	m_buyButton.setFillColor(sf::Color(255, 206, 43));
	m_buyButton.setOrigin(sf::Vector2f(-800, -200));

	m_buyButtonText.setString("Buy");
	m_buyButtonText.setFont(m_buttonFont);
	m_buyButtonText.setCharacterSize(17);
	m_buyButtonText.setFillColor(sf::Color(36, 33, 48));
	m_buyButtonText.setOrigin(sf::Vector2f(-805, -205));
}

void WishList::RunFrame()
{
	CreateFrame();
	while (m_wishFrame.isOpen())
	{
		m_wishFrame.clear(sf::Color(255, 255, 255));

		sf::Event event;
		while (m_wishFrame.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_wishFrame.close();
		}

		DrawElements();
		m_wishFrame.display();
		m_wishFrame.setActive();
	}
}

void WishList::DrawElements()
{
	m_wishFrame.draw(m_menuBar);
	m_wishFrame.draw(m_logo);
}
