#pragma once
#include <string>

class User
{
public:
	User(const std::string& firstName, const std::string& lastName, const std::string& email, const std::string& username, const std::string& password);

	const std::string& GetFirstName() const;
	const std::string& GetLastName() const;
	const std::string& GetEmail() const;
	const std::string& GetUsername() const;
	const std::string& GetPassword() const;

private:
	std::string m_firstName;
	std::string m_lastName;
	std::string m_email;
	std::string m_username;
	std::string m_password;
};

