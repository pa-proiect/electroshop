#include "LoginFrame.h"
#include <iostream>
#include <string>

LoginFrame::LoginFrame()
{
}

void LoginFrame::SetTCP(const std::shared_ptr<TCPClient>& tcp)
{
	m_tcp = tcp;
}

void LoginFrame::SetMenuBar()
{
	m_menuBar.setSize(sf::Vector2f(480, 70));
	m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void LoginFrame::InitializeFonts()
{
	if (!m_logoFont.loadFromFile("LogoFont.ttf"))
		std::cerr << "LogoFont can't be loaded!" << std::endl;

	if (!m_buttonFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void LoginFrame::SetLogo()
{
	m_logo.setString("ElectroShop");
	m_logo.setFont(m_logoFont);
	m_logo.setCharacterSize(45);
	m_logo.setFillColor(sf::Color(255, 206, 43));
	m_logo.setOrigin(sf::Vector2f(-120, 0));
}

void LoginFrame::SetLables()
{
	m_mailLable.setString("E-mail");
	m_mailLable.setFont(m_buttonFont);
	m_mailLable.setFillColor(sf::Color::Black);
	m_mailLable.setCharacterSize(17);
	m_mailLable.setOrigin(sf::Vector2f(-5, -180));

	m_passwordLable.setString("Password");
	m_passwordLable.setFont(m_buttonFont);
	m_passwordLable.setFillColor(sf::Color::Black);
	m_passwordLable.setCharacterSize(17);
	m_passwordLable.setOrigin(sf::Vector2f(-5, -290));
}

void LoginFrame::SetInput(const std::string& type, sf::Vector2f origin)
{
	if (ParseStringToInputType(type) == InputType::Email)
	{
		m_mail.SetType(type);
		m_mail.SetOrigin(origin);
		m_mail.SetTextOrigin(sf::Vector2f(-105, -180));
	}
	if (ParseStringToInputType(type) == InputType::Password)
	{
		m_password.SetType(type);
		m_password.SetOrigin(origin);
		m_password.SetTextOrigin(sf::Vector2f(-105, -290));
	}
}

void LoginFrame::SetLoginButton()
{
	m_loginButton.setSize(sf::Vector2f(90, 40));
	m_loginButton.setFillColor(sf::Color(255, 206, 43));
	m_loginButton.setOrigin(sf::Vector2f(-200, -400));

	m_loginText.setString("Log In");
	m_loginText.setFont(m_buttonFont);
	m_loginText.setCharacterSize(17);
	m_loginText.setFillColor(sf::Color(36, 33, 48));
	m_loginText.setOrigin(sf::Vector2f(-216, -406));
}

void LoginFrame::CreateFrame()
{
	m_loginFrame.create(sf::VideoMode(480, 640), "Electro Shop");

	SetMenuBar();

	InitializeFonts();

	SetLogo();
	SetLables();

	SetInput("Email", sf::Vector2f(-100, -170));
	SetInput("Password", sf::Vector2f(-100, -280));

	SetLoginButton();
}

void LoginFrame::RunFrame()
{
	CreateFrame();
	bool mailSelected = false;
	bool passwordSelected = false;

	while (m_loginFrame.isOpen())
	{
		m_loginFrame.clear(sf::Color(255, 255, 255));

		sf::Event event;
		while (m_loginFrame.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_loginFrame.close();
			if (PressedTextBox(m_mail.GetTextBox()))
			{
				mailSelected = true;
				passwordSelected = false;
			}
			if (PressedTextBox(m_password.GetTextBox()))
			{
				mailSelected = false;
				passwordSelected = true;
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !PressedTextBox(m_mail.GetTextBox()) && !PressedTextBox(m_password.GetTextBox()))
			{
				mailSelected = false;
				passwordSelected = false;
				
				std::string loginData = "~" + m_mail.GetText().getString() + m_password.GetText().getString();

				if (m_tcp->GetLogged())
				{
					std::cout << "You are logged";
				}
			}
			if (event.type == sf::Event::TextEntered)
			{
				if (mailSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String mail = static_cast<sf::String>(character);
					m_mail.SetInput(mail);
					m_mail.SetTextFont(m_buttonFont);
				}
				if (passwordSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String password = static_cast<sf::String>(character);
					m_password.SetInput(password);
					m_password.SetTextFont(m_buttonFont);
				}
			}
		}
		DrawElements();

		m_loginFrame.display();
		m_loginFrame.setActive();
	}
}

void LoginFrame::DrawElements()
{
	m_loginFrame.draw(m_menuBar);
	m_loginFrame.draw(m_logo);
	m_loginFrame.draw(m_mail.GetTextBox());
	m_loginFrame.draw(m_password.GetTextBox());
	m_loginFrame.draw(m_loginButton);
	m_loginFrame.draw(m_loginText);
	m_loginFrame.draw(m_mail.GetText());
	m_loginFrame.draw(m_password.GetText());
	m_loginFrame.draw(m_mailLable);
	m_loginFrame.draw(m_passwordLable);
}

const bool LoginFrame::PressedTextBox(const sf::RectangleShape& textBox) const
{
	sf::FloatRect floatRect = textBox.getGlobalBounds();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		float x = static_cast<float>(sf::Mouse::getPosition(m_loginFrame).x);
		float y = static_cast<float>(sf::Mouse::getPosition(m_loginFrame).y);
		if (floatRect.contains(x, y))
		{
			return true;
		}
	}
	return false;
}
