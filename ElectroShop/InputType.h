#pragma once
#include <string>

enum class InputType :int16_t
{
	FirstName = 10,
	LastName,
	Email,
	Address,
	Username,
	Password,
	Repassword,
	ProductName,
	ProductPrice,
	ProductRetailer,
	ProductType
};

inline InputType ParseStringToInputType(const std::string& type)
{
	if (type == "FirstName")
	{
		return InputType::FirstName;
	}

	if (type == "LastName")
	{
		return InputType::LastName;
	}

	if (type == "Email")
	{
		return InputType::Email;
	}

	if (type == "Address")
	{
		return InputType::Address;
	}

	if (type == "Username")
	{
		return InputType::Username;
	}

	if (type == "Password")
	{
		return InputType::Password;
	}

	if (type == "Repassword")
	{
		return InputType::Repassword;
	}

	if (type == "ProductName")
	{
		return InputType::ProductName;
	}

	if (type == "ProductPrice")
	{
		return InputType::ProductPrice;
	}

	if (type == "ProductRetailer")
	{
		return InputType::ProductRetailer;
	}

	if (type == "ProductType")
	{
		return InputType::ProductType;
	}
}