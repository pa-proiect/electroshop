#include "RegisterFrame.h"
#include <iostream>

RegisterFrame::RegisterFrame()
{
}

void RegisterFrame::SetTCP(const std::shared_ptr<TCPClient>& tcp)
{
	m_tcp = tcp;
}

void RegisterFrame::SetMenuBar()
{
	m_menuBar.setSize(sf::Vector2f(640, 70));
	m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void RegisterFrame::InitializeFonts()
{
	if (!m_logoFont.loadFromFile("LogoFont.ttf"))
		std::cerr << "LogoFont can't be loaded!" << std::endl;

	if (!m_buttonFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void RegisterFrame::SetLogo()
{
	m_logo.setString("ElectroShop");
	m_logo.setFont(m_logoFont);
	m_logo.setCharacterSize(45);
	m_logo.setFillColor(sf::Color(255, 206, 43));
	m_logo.setOrigin(sf::Vector2f(-190, 0));
}

void RegisterFrame::SetLables()
{
	m_firstNameLable.setString("First Name");
	m_firstNameLable.setFont(m_buttonFont);
	m_firstNameLable.setFillColor(sf::Color::Black);
	m_firstNameLable.setCharacterSize(17);
	m_firstNameLable.setOrigin(sf::Vector2f(-50, -180));

	m_lastNameLable.setString("Last Name");
	m_lastNameLable.setFont(m_buttonFont);
	m_lastNameLable.setFillColor(sf::Color::Black);
	m_lastNameLable.setCharacterSize(17);
	m_lastNameLable.setOrigin(sf::Vector2f(-50, -230));

	m_mailLable.setString("E-mail");
	m_mailLable.setFont(m_buttonFont);
	m_mailLable.setFillColor(sf::Color::Black);
	m_mailLable.setCharacterSize(17);
	m_mailLable.setOrigin(sf::Vector2f(-50, -280));

	m_addressLable.setString("Address");
	m_addressLable.setFont(m_buttonFont);
	m_addressLable.setFillColor(sf::Color::Black);
	m_addressLable.setCharacterSize(17);
	m_addressLable.setOrigin(sf::Vector2f(-50, -330));

	m_usernameLable.setString("Username");
	m_usernameLable.setFont(m_buttonFont);
	m_usernameLable.setFillColor(sf::Color::Black);
	m_usernameLable.setCharacterSize(17);
	m_usernameLable.setOrigin(sf::Vector2f(-50, -380));

	m_passwordLable.setString("Password");
	m_passwordLable.setFont(m_buttonFont);
	m_passwordLable.setFillColor(sf::Color::Black);
	m_passwordLable.setCharacterSize(17);
	m_passwordLable.setOrigin(sf::Vector2f(-50, -430));

	m_repasswordLable.setString("Repassword");
	m_repasswordLable.setFont(m_buttonFont);
	m_repasswordLable.setFillColor(sf::Color::Black);
	m_repasswordLable.setCharacterSize(17);
	m_repasswordLable.setOrigin(sf::Vector2f(-50, -480));
}

void RegisterFrame::SetInput(const std::string& type, sf::Vector2f origin)
{
	if (ParseStringToInputType(type) == InputType::FirstName)
	{
		m_firstName.SetType(type);
		m_firstName.SetOrigin(origin);
		m_firstName.SetTextOrigin(sf::Vector2f(-175, -180));
	}
	if (ParseStringToInputType(type) == InputType::LastName)
	{
		m_lastName.SetType(type);
		m_lastName.SetOrigin(origin);
		m_lastName.SetTextOrigin(sf::Vector2f(-175, -230));
	}
	if (ParseStringToInputType(type) == InputType::Email)
	{
		m_email.SetType(type);
		m_email.SetOrigin(origin);
		m_email.SetTextOrigin(sf::Vector2f(-175, -280));
	}
	if (ParseStringToInputType(type) == InputType::Address)
	{
		m_address.SetType(type);
		m_address.SetOrigin(origin);
		m_address.SetTextOrigin(sf::Vector2f(-175, -330));
	}
	if (ParseStringToInputType(type) == InputType::Username)
	{
		m_username.SetType(type);
		m_username.SetOrigin(origin);
		m_username.SetTextOrigin(sf::Vector2f(-175, -380));
	}
	if (ParseStringToInputType(type) == InputType::Password)
	{
		m_password.SetType(type);
		m_password.SetOrigin(origin);
		m_password.SetTextOrigin(sf::Vector2f(-175, -430));
	}
	if (ParseStringToInputType(type) == InputType::Repassword)
	{
		m_repassword.SetType(type);
		m_repassword.SetOrigin(origin);
		m_repassword.SetTextOrigin(sf::Vector2f(-175, -480));
	}
}

void RegisterFrame::SetRegisterButton()
{
	m_registerButton.setSize(sf::Vector2f(90, 40));
	m_registerButton.setFillColor(sf::Color(255, 206, 43));
	m_registerButton.setOrigin(sf::Vector2f(-270, -540));

	m_registerText.setString("Register");
	m_registerText.setFont(m_buttonFont);
	m_registerText.setCharacterSize(17);
	m_registerText.setFillColor(sf::Color(36, 33, 48));
	m_registerText.setOrigin(sf::Vector2f(-275, -545));
}

void RegisterFrame::SetLoginButton()
{
	m_haveAccount.setString("Already have an account?");
	m_haveAccount.setFont(m_buttonFont);
	m_haveAccount.setFillColor(sf::Color::Black);
	m_haveAccount.setCharacterSize(14);
	m_haveAccount.setOrigin(sf::Vector2f(-220, -670));

	m_loginButton.setSize(sf::Vector2f(70, 30));
	m_loginButton.setFillColor(sf::Color(80,80,80));
	m_loginButton.setOrigin(sf::Vector2f(-270, -700));

	m_loginText.setString("Log in");
	m_loginText.setFont(m_buttonFont);
	m_loginText.setCharacterSize(15);
	m_loginText.setFillColor(sf::Color(255, 206, 43));
	m_loginText.setOrigin(sf::Vector2f(-275, -705));
}

void RegisterFrame::SetInvalidPassword()
{
	m_textInvalidPassword.setOrigin(sf::Vector2f(-100, -590));
	m_textInvalidPassword.setCharacterSize(12);
	m_textInvalidPassword.setFillColor(sf::Color::Red);
	m_textInvalidPassword.setFont(m_buttonFont);
}

void RegisterFrame::SetInvalidMailText()
{
	m_textInvalidMail.setOrigin(sf::Vector2f(-100, -610));
	m_textInvalidMail.setCharacterSize(12);
	m_textInvalidMail.setFillColor(sf::Color::Red);
	m_textInvalidMail.setFont(m_buttonFont);
}

void RegisterFrame::SetInvalidUsernameText()
{
	m_textInvalidUsername.setOrigin(sf::Vector2f(-100, -630));
	m_textInvalidUsername.setCharacterSize(12);
	m_textInvalidUsername.setFillColor(sf::Color::Red);
	m_textInvalidUsername.setFont(m_buttonFont);
}

void RegisterFrame::SetInvalidRepassword()
{
	m_invalidRepassword.setOrigin(sf::Vector2f(-100, -650));
	m_invalidRepassword.setCharacterSize(12);
	m_invalidRepassword.setFillColor(sf::Color::Red);
	m_invalidRepassword.setFont(m_buttonFont);
}

const sf::RectangleShape& RegisterFrame::GetLoginButton() const
{
	return m_loginButton;
}

const bool RegisterFrame::PressButton(const sf::RectangleShape& button) const
{
	sf::FloatRect floatRect = button.getGlobalBounds();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		float x = static_cast<float>(sf::Mouse::getPosition(m_registerFrame).x);
		float y = static_cast<float>(sf::Mouse::getPosition(m_registerFrame).y);
		if (floatRect.contains(x, y))
		{
			return true;
		}
	}
	return false;
}

void RegisterFrame::CreateFrame()
{
	m_registerFrame.create(sf::VideoMode(640, 800), "Electro Shop");

	SetMenuBar();

	InitializeFonts();

	SetLogo();

	SetLables();

	SetInput("FirstName", sf::Vector2f(-170, -170));
	SetInput("LastName", sf::Vector2f(-170, -220));
	SetInput("Email", sf::Vector2f(-170, -270));
	SetInput("Address", sf::Vector2f(-170, -320));
	SetInput("Username", sf::Vector2f(-170, -370));
	SetInput("Password", sf::Vector2f(-170, -420));
	SetInput("Repassword", sf::Vector2f(-170, -470));
	
	SetInvalidPassword();
	SetInvalidMailText();
	SetInvalidUsernameText();
	SetInvalidRepassword();

	SetRegisterButton();
	SetLoginButton();
}

void RegisterFrame::RunFrame()
{
	CreateFrame();

	bool firstNameSelected = false;
	bool lastNameSelected = false;
	bool emailSelected = false;
	bool addressSelected = false;
	bool usernameSelected = false;
	bool passwordSelected = false;
	bool repasswordSelected = false;

	while (m_registerFrame.isOpen())
	{
		m_registerFrame.clear(sf::Color(255, 255, 255));

		sf::Event event;
		while (m_registerFrame.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_registerFrame.close();
			if (PressButton(m_registerButton))
			{
				bool lengthOrUpperCase;
				if (!(CheckPassword(m_password.GetText(), lengthOrUpperCase)))
				{
					if (lengthOrUpperCase)
					{
						m_textInvalidPassword.setString("Your password should contain at least 9 characters!");
					}
					else
					{
						m_textInvalidPassword.setString("Your password should contain at least 1 uppercase charcter and a number!");
					}
				}
				if (!(CheckEmail(m_email.GetText())))
				{
					m_textInvalidMail.setString("Invalid e-mail. Please, try again!");
				}
				if (!(CheckUsername(m_username.GetText())))
				{
					m_textInvalidUsername.setString("Your username should contain at the end &Retailer or &Buyer");
				}
				if (!(CheckRepassword(m_password.GetText(), m_repassword.GetText())))
				{
					m_invalidRepassword.setString("Your password and repassword are not the same!");
				}
			}
			if (PressButton(m_loginButton))
			{
				m_registerFrame.close();
				m_loginFrame.RunFrame();
			}
			if (PressedTextBox(m_firstName.GetTextBox()))
			{
				firstNameSelected = true;
				lastNameSelected = false;
				emailSelected = false;
				addressSelected = false;
				usernameSelected = false;
				passwordSelected = false;
				repasswordSelected = false;
			}
			if (PressedTextBox(m_lastName.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = true;
				emailSelected = false;
				addressSelected = false;
				usernameSelected = false;
				passwordSelected = false;
				repasswordSelected = false;
			}
			if (PressedTextBox(m_email.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = false;
				emailSelected = true;
				addressSelected = false;
				usernameSelected = false;
				passwordSelected = false;
				repasswordSelected = false;
			}
			if (PressedTextBox(m_address.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = false;
				emailSelected = false;
				addressSelected = true;
				usernameSelected = false;
				passwordSelected = false;
				repasswordSelected = false;
			}
			if (PressedTextBox(m_username.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = false;
				emailSelected = false;
				addressSelected = false;
				usernameSelected = true;
				passwordSelected = false;
				repasswordSelected = false;
			}
			if (PressedTextBox(m_password.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = false;
				emailSelected = false;
				addressSelected = false;
				usernameSelected = false;
				passwordSelected = true;
				repasswordSelected = false;
			}
			if (PressedTextBox(m_repassword.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = false;
				emailSelected = false;
				addressSelected = false;
				usernameSelected = false;
				passwordSelected = false;
				repasswordSelected = true;
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && 
				!PressedTextBox(m_firstName.GetTextBox()) && 
				!PressedTextBox(m_lastName.GetTextBox()) &&
				!PressedTextBox(m_email.GetTextBox()) &&
				!PressedTextBox(m_address.GetTextBox()) &&
				!PressedTextBox(m_username.GetTextBox()) &&
				!PressedTextBox(m_password.GetTextBox()) &&
				!PressedTextBox(m_repassword.GetTextBox()))
			{
				firstNameSelected = false;
				lastNameSelected = false;
				emailSelected = false;
				addressSelected = false;
				usernameSelected = false;
				passwordSelected = false;
				repasswordSelected = false;

				std::string registerData;
				registerData.append(m_email.GetText().getString() + "-");
				registerData.append(m_password.GetText().getString() + " ");
				registerData.append(m_firstName.GetText().getString() + "-");
				registerData.append(m_lastName.GetText().getString() + "-");
				registerData.append(m_email.GetText().getString() + "-");
				registerData.append(m_address.GetText().getString() + "-");
				registerData.append(m_password.GetText().getString());

				m_tcp->SendRegisterData(registerData);
			}
			if (event.type == sf::Event::TextEntered)
			{
				if (firstNameSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String firstName = static_cast<sf::String>(character);
					m_firstName.SetInput(firstName);
					m_firstName.SetTextFont(m_buttonFont);
				}
				if (lastNameSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String lastName = static_cast<sf::String>(character);
					m_lastName.SetInput(lastName);
					m_lastName.SetTextFont(m_buttonFont);
				}
				if (emailSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String email = static_cast<sf::String>(character);
					m_email.SetInput(email);
					m_email.SetTextFont(m_buttonFont);
				}
				if (addressSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String address = static_cast<sf::String>(character);
					m_address.SetInput(address);
					m_address.SetTextFont(m_buttonFont);
				}
				if (usernameSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String username = static_cast<sf::String>(character);
					m_username.SetInput(username);
					m_username.SetTextFont(m_buttonFont);
				}
				if (passwordSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String password = static_cast<sf::String>(character);
					m_password.SetInput(password);
					m_password.SetTextFont(m_buttonFont);
				}
				if (repasswordSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String repassword = static_cast<sf::String>(character);
					m_repassword.SetInput(repassword);
					m_repassword.SetTextFont(m_buttonFont);
				}
			}
		}
		DrawElements();

		m_registerFrame.display();
		m_registerFrame.setActive();
	}
}

void RegisterFrame::DrawElements()
{
	m_registerFrame.draw(m_menuBar);
	m_registerFrame.draw(m_logo);
	m_registerFrame.draw(m_firstName.GetTextBox());
	m_registerFrame.draw(m_lastName.GetTextBox());
	m_registerFrame.draw(m_email.GetTextBox());
	m_registerFrame.draw(m_address.GetTextBox());
	m_registerFrame.draw(m_username.GetTextBox());
	m_registerFrame.draw(m_password.GetTextBox());
	m_registerFrame.draw(m_repassword.GetTextBox());
	m_registerFrame.draw(m_registerButton);
	m_registerFrame.draw(m_registerText);
	m_registerFrame.draw(m_haveAccount);
	m_registerFrame.draw(m_loginButton);
	m_registerFrame.draw(m_loginText);
	m_registerFrame.draw(m_firstNameLable);
	m_registerFrame.draw(m_lastNameLable);
	m_registerFrame.draw(m_mailLable);
	m_registerFrame.draw(m_addressLable);
	m_registerFrame.draw(m_usernameLable);
	m_registerFrame.draw(m_passwordLable);
	m_registerFrame.draw(m_repasswordLable);
	m_registerFrame.draw(m_firstName.GetText());
	m_registerFrame.draw(m_lastName.GetText());
	m_registerFrame.draw(m_email.GetText());
	m_registerFrame.draw(m_address.GetText());
	m_registerFrame.draw(m_username.GetText());
	m_registerFrame.draw(m_password.GetText());
	m_registerFrame.draw(m_repassword.GetText());
	m_registerFrame.draw(m_textInvalidPassword);
	m_registerFrame.draw(m_textInvalidMail);
	m_registerFrame.draw(m_textInvalidUsername);
	m_registerFrame.draw(m_invalidRepassword);
}

const bool RegisterFrame::CheckPassword(const sf::Text& passwordText, bool& lengthOrUpperCase) const
{
	bool upperCase = false;
	bool numberCase = false;
	bool ok = false;

	std::regex upperCaseRegex{ "[A-Z]+" };
	std::regex numberCaseRegex{ "[0-9]+" };
	std::string password = passwordText.getString();

	if (password.length() < 9)
	{
		lengthOrUpperCase = true;
		ok = false;
	}
	else
	{
		upperCase = std::regex_search(password, upperCaseRegex);
		numberCase = std::regex_search(password, numberCaseRegex);

		int counter = upperCase + numberCase;
		if (counter < 2)
		{
			ok = false;
			lengthOrUpperCase = false;
		}
		else
		{
			ok = true;
		}
	}
	return ok;
}

const bool RegisterFrame::CheckEmail(const sf::Text& emailText) const
{
	std::string mail = emailText.getString();
	const std::regex expression("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");

	return std::regex_match(mail, expression);
}

const bool RegisterFrame::CheckUsername(const sf::Text& usernameText) const
{
	std::string username = usernameText.getString();
	const std::regex expression("(\\w+)&(\\Retailer|Buyer)");

	return std::regex_match(username, expression);
}

const bool RegisterFrame::CheckRepassword(const sf::Text& passwordText, const sf::Text& repasswordText) const
{
	std::string password = passwordText.getString();
	std::string repassword = repasswordText.getString();
	if (password.compare(repassword) == 0)
	{
		return true;
	}
	return false;
}

const bool RegisterFrame::PressedTextBox(const sf::RectangleShape& textBox) const
{
	sf::FloatRect floatRect = textBox.getGlobalBounds();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		float x = static_cast<float>(sf::Mouse::getPosition(m_registerFrame).x);
		float y = static_cast<float>(sf::Mouse::getPosition(m_registerFrame).y);
		if (floatRect.contains(x, y))
		{
			return true;
		}
	}
	return false;
}
