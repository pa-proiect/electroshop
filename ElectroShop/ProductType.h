#pragma once
#include <string>

enum class ProductType :int16_t
{
    Laptop = 10,
    Phone,
    HeadPhone,
    SpeakerSystem,
    TV,
    Camera,
    Accesories,
};

inline ProductType ParseStringToProductType(const std::string& type)
{

    if (type == "Laptop")
    {
        return ProductType::Laptop;
    }

    if (type == "Phone")
    {
        return ProductType::Phone;
    }

    if (type == "HeadPhone")
    {
        return ProductType::HeadPhone;
    }

    if (type == "SpeakerSystem")
    {
        return ProductType::SpeakerSystem;
    } 

    if (type == "TV")
    {
        return ProductType::TV;
    }

    if (type == "Camera")
    {
        return ProductType::Camera;
    }

    if (type == "Accesories")
    {
        return ProductType::Accesories;
    }
}