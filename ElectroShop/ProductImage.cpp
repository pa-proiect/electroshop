#include "ProductImage.h"
#include <iostream>

ProductImage::ProductImage(const double price, const std::string& sellerName)
	:m_price(price), m_sellerName(sellerName)
{
}

ProductImage::ProductImage(const ProductImage& productImage)
{
	*this = productImage;
}

ProductImage::ProductImage(ProductImage&& productImage)
{
	*this = std::move(productImage);
}

ProductImage& ProductImage::operator=(const ProductImage& productImage)
{
	m_image = productImage.m_image;
	m_price = productImage.m_price;
	m_sellerName = productImage.m_sellerName;
	return *this;
}

ProductImage& ProductImage::operator=(ProductImage&& productImage)
{
	m_image = productImage.m_image;
	m_price = productImage.m_price;
	m_sellerName = productImage.m_sellerName;
	new(&productImage) ProductImage;
	return *this;
}

void ProductImage::SetImage(std::unique_ptr<sf::Texture> image)
{
	m_image = *image.get();
}

const sf::Texture& ProductImage::GetImage() const
{
	return m_image;
}

const double ProductImage::GetPrice() const
{
	return m_price;
}

const std::string& ProductImage::GetSellerName() const
{
	return m_sellerName;
}
