#pragma once
#include <SFML/Graphics.hpp>
#include "InputTextBox.h"
#include "ProductType.h"

class AddProductFrame
{
public:
	AddProductFrame();

	void SetMenuBar();
	void InitializeFonts();
	void SetLogo();
	void SetLables();
	void SetInput(const std::string& type, sf::Vector2f origin);
	void SetAddButton();
	void CreateFrame();
	void RunFrame();
	void DrawElements();

	const bool PressedTextBox(const sf::RectangleShape& textBox) const;
private:
	sf::RenderWindow m_addProductFrame;
	sf::RectangleShape m_menuBar;
	sf::Font m_logoFont;
	sf::Text m_logo;
	InputTextBox m_name;
	InputTextBox m_price;
	InputTextBox m_retailer;
	InputTextBox m_type;
	sf::Text m_nameLable;
	sf::Text m_priceLable;
	sf::Text m_retailerLable;
	sf::Text m_typeLable;
	sf::Font m_buttonFont;
	sf::RectangleShape m_addButton;
	sf::Text m_addButtonText;
};

