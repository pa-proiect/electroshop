#include "TCPClient.h"
#include <iostream>

TCPClient::TCPClient()
{
	m_logged = false;
}

void TCPClient::InitializeWinsock()
{
    m_initialize = WSAStartup(MAKEWORD(2, 2), &m_wsaData);
	if (m_initialize != 0)
	{
		std::cerr << "WSAStartup failed!" << std::endl;
	}
}

void TCPClient::CreateClientSocket()
{
	m_connectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_connectSocket == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket";
	}
}

void TCPClient::ConnectToServer()
{
	std::string ipAddress = "127.0.0.1";
	m_hint.sin_family = AF_INET;
	m_hint.sin_port = htons(54000);
	inet_pton(AF_INET, ipAddress.c_str(), &m_hint.sin_addr);

	int connectResult = connect(m_connectSocket, (sockaddr*)&m_hint, sizeof(m_hint));
	if (connectResult == SOCKET_ERROR)
	{
		std::cerr << "Can't connect to server, Err # " << WSAGetLastError << std::endl;
		closesocket(m_connectSocket);
		WSACleanup();
	}
}

void TCPClient::SendLoginData(std::string& data)
{
	char buf[4096];
	int sendData = send(m_connectSocket, data.c_str(), data.size(), 0);
	
	ZeroMemory(buf, 4096);
	int bytesReceived = recv(m_connectSocket, buf, 4096, 0);
	if (bytesReceived == SOCKET_ERROR)
	{
		std::cerr << "Error in recv!. Quitting!" << std::endl;
	}

	if (std::string(buf, 0, bytesReceived) == "true")
	{
		m_logged = true;
	}
	else
	{
		m_logged = false;
	}
}

void TCPClient::SendRegisterData(std::string& data)
{
	int sendData = send(m_connectSocket, data.c_str(), data.size() + 1, 0);
}

void TCPClient::CloseSocket()
{
	closesocket(m_connectSocket);
	WSACleanup();
}

void TCPClient::Run()
{
	InitializeWinsock();
	CreateClientSocket();
	ConnectToServer();
}

const bool TCPClient::GetLogged() const
{
	return m_logged;
}


