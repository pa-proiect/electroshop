#include "Product.h"

Product::Product(const std::string& name, double price, const std::string& info, ProductType type, ProductBrand brand) 
	:m_name(name), m_price(price), m_info(info), m_type(type), m_brand(brand)
{
}

const std::string& Product::GetProductName() const
{
	return m_name;
}

const double Product::GetProductPrice() const
{
	return m_price;
}

const std::string& Product::GetProductInfo() const
{
	return m_info;
}

ProductType Product::GetProductType() const
{
	return m_type;
}

ProductBrand Product::GetProductBrand() const
{
	return m_brand;
}

const bool Product::operator==(const Product product) const
{
	if (m_name != product.GetProductName())
		return false;

	if (m_price != product.GetProductPrice())
		return false; 

	if (m_info != product.GetProductInfo())
		return false;

	if (m_type != product.GetProductType())
		return false;

	if (m_brand != product.GetProductBrand())
		return false;
	
	return true;
}


