#pragma once
#include "ProductImage.h"
#include "Product.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>


class ProductFrame
{
public:
	ProductFrame() = default;
	ProductFrame(Product product, ProductImage productImage);

	ProductImage GetProductImage() const;
	Product GetProduct() const;
	const bool GetIfPressCart() const;
	const bool GetIfPressWish() const;

	void AddImage();
	void SetCartButton();
	void SetWishButton();
	void SetRectangle();
	void SetImage(const sf::Texture& texture);
	void SetName(const std::string& name);
	void SetPrice(const std::string& price);
	void SetRetailer(const std::string& retailer);

	void CreateFrame();
	void SetMenuBar();
	void InitializeFonts();
	void SetLogo();
	void RunFrame();
	void DrawElements();

	const bool PressButton(sf::RectangleShape& button) const;

private:
	sf::RenderWindow m_productFrame;
	sf::RenderWindow m_cartFrame;
	sf::RectangleShape m_menuBar;
	sf::Font m_logoFont;
	sf::Text m_logo;
	sf::Font m_buttonFont;
	ProductImage m_productImage;
	Product m_product;
	sf::RectangleShape m_cartButton;
	sf::RectangleShape m_wishButton;
	sf::Text m_cartButtonText;
	sf::Text m_wishButtonText;
	sf::RectangleShape m_rectangle;
	sf::Sprite m_image;
	sf::Text m_prodName;
	sf::Text m_prodPrice;
	sf::Text m_prodRetailer;
	bool m_cartPress;
	bool m_wishPress;
};

