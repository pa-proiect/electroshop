#pragma once
#include <string>
#include "ProductType.h"
#include "ProductBrand.h"

class Product
{
public:
	Product() = default;
	Product(const std::string& name, double price, const std::string& info, ProductType type, ProductBrand brand);

	const std::string& GetProductName() const;
	const double GetProductPrice() const;
	const std::string& GetProductInfo() const;
	ProductType GetProductType() const;
	ProductBrand GetProductBrand() const;

	const bool operator==(const Product product) const;

private:
	std::string m_name;
	double m_price;
	std::string m_info;
	ProductType m_type;
	ProductBrand m_brand;
};
