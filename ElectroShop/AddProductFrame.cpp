#include "AddProductFrame.h"
#include <iostream>

AddProductFrame::AddProductFrame()
{
}

void AddProductFrame::SetMenuBar()
{
	m_menuBar.setSize(sf::Vector2f(480, 70));
	m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void AddProductFrame::InitializeFonts()
{
	if (!m_logoFont.loadFromFile("LogoFont.ttf"))
		std::cerr << "LogoFont can't be loaded!" << std::endl;

	if (!m_buttonFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void AddProductFrame::SetLogo()
{
	m_logo.setString("ElectroShop");
	m_logo.setFont(m_logoFont);
	m_logo.setCharacterSize(45);
	m_logo.setFillColor(sf::Color(255, 206, 43));
	m_logo.setOrigin(sf::Vector2f(-120, 0));
}

void AddProductFrame::SetLables()
{
	m_nameLable.setString("Product Name");
	m_nameLable.setFont(m_buttonFont);
	m_nameLable.setFillColor(sf::Color::Black);
	m_nameLable.setCharacterSize(17);
	m_nameLable.setOrigin(sf::Vector2f(-5, -130));

	m_priceLable.setString("Product Price");
	m_priceLable.setFont(m_buttonFont);
	m_priceLable.setFillColor(sf::Color::Black);
	m_priceLable.setCharacterSize(17);
	m_priceLable.setOrigin(sf::Vector2f(-5, -240));

	m_retailerLable.setString("Retailer");
	m_retailerLable.setFont(m_buttonFont);
	m_retailerLable.setFillColor(sf::Color::Black);
	m_retailerLable.setCharacterSize(17);
	m_retailerLable.setOrigin(sf::Vector2f(-5, -350));

	m_typeLable.setString("Product Type");
	m_typeLable.setFont(m_buttonFont);
	m_typeLable.setFillColor(sf::Color::Black);
	m_typeLable.setCharacterSize(17);
	m_typeLable.setOrigin(sf::Vector2f(-5, -460));
}

void AddProductFrame::SetInput(const std::string& type, sf::Vector2f origin)
{
	if (ParseStringToInputType(type) == InputType::ProductName)
	{
		m_name.SetType(type);
		m_name.SetOrigin(origin);
		m_name.SetTextOrigin(sf::Vector2f(-155, -130));
	}
	if (ParseStringToInputType(type) == InputType::ProductPrice)
	{
		m_price.SetType(type);
		m_price.SetOrigin(origin);
		m_price.SetTextOrigin(sf::Vector2f(-155, -240));
	}
	if (ParseStringToInputType(type) == InputType::ProductRetailer)
	{
		m_retailer.SetType(type);
		m_retailer.SetOrigin(origin);
		m_retailer.SetTextOrigin(sf::Vector2f(-155, -350));
	}
	if (ParseStringToInputType(type) == InputType::ProductType)
	{
		m_type.SetType(type);
		m_type.SetOrigin(origin);
		m_type.SetTextOrigin(sf::Vector2f(-155, -460));
	}
}

void AddProductFrame::SetAddButton()
{
	m_addButton.setSize(sf::Vector2f(125, 35));
	m_addButton.setFillColor(sf::Color(255, 206, 43));
	m_addButton.setOrigin(sf::Vector2f(-180, -540));

	m_addButtonText.setString("Add Product");
	m_addButtonText.setFont(m_buttonFont);
	m_addButtonText.setCharacterSize(17);
	m_addButtonText.setFillColor(sf::Color(36, 33, 48));
	m_addButtonText.setOrigin(sf::Vector2f(-185, -546));
}

void AddProductFrame::CreateFrame()
{
	m_addProductFrame.create(sf::VideoMode(480, 640), "Electro Shop");

	SetMenuBar();

	InitializeFonts();

	SetLogo();
	SetLables();

	SetInput("ProductName", sf::Vector2f(-150, -120));
	SetInput("ProductPrice", sf::Vector2f(-150, -230));
	SetInput("ProductRetailer", sf::Vector2f(-150, -340));
	SetInput("ProductType", sf::Vector2f(-150, -450));

	SetAddButton();
}

void AddProductFrame::RunFrame()
{
	CreateFrame();
	bool nameSelected = false;
	bool priceSelected = false;
	bool retailerSelected = false;
	bool typeSelected = false;

	while (m_addProductFrame.isOpen())
	{
		m_addProductFrame.clear(sf::Color(255, 255, 255));

		sf::Event event;
		while (m_addProductFrame.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_addProductFrame.close();
			if (PressedTextBox(m_name.GetTextBox()))
			{
				nameSelected = true;
				priceSelected = false;
				retailerSelected = false;
				typeSelected = false;
			}
			if (PressedTextBox(m_price.GetTextBox()))
			{
				nameSelected = false;
				priceSelected = true;
				retailerSelected = false;
				typeSelected = false;
			}
			if (PressedTextBox(m_retailer.GetTextBox()))
			{
				nameSelected = false;
				priceSelected = false;
				retailerSelected = true;
				typeSelected = false;
			}
			if (PressedTextBox(m_type.GetTextBox()))
			{
				nameSelected = false;
				priceSelected = false;
				retailerSelected = false;
				typeSelected = true;
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && 
				!PressedTextBox(m_name.GetTextBox()) && 
				!PressedTextBox(m_price.GetTextBox())&&
				!PressedTextBox(m_retailer.GetTextBox())&&
				!PressedTextBox(m_type.GetTextBox()))
			{
				nameSelected = false;
				priceSelected = false;
				retailerSelected = false;
				typeSelected = false;
			}
			if (event.type == sf::Event::TextEntered)
			{
				if (nameSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String name = static_cast<sf::String>(character);
					m_name.SetInput(name);
					m_name.SetTextFont(m_buttonFont);
				}
				if (priceSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String price = static_cast<sf::String>(character);
					m_price.SetInput(price);
					m_price.SetTextFont(m_buttonFont);
				}
				if (retailerSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String retailer = static_cast<sf::String>(character);
					m_retailer.SetInput(retailer);
					m_retailer.SetTextFont(m_buttonFont);
				}
				if (typeSelected)
				{
					char character = static_cast<char>(event.text.unicode);
					sf::String type = static_cast<sf::String>(character);
					m_type.SetInput(type);
					m_type.SetTextFont(m_buttonFont);
				}
			}
		}
		DrawElements();

		m_addProductFrame.display();
		m_addProductFrame.setActive();
	}
}

void AddProductFrame::DrawElements()
{
	m_addProductFrame.draw(m_menuBar);
	m_addProductFrame.draw(m_logo);
	m_addProductFrame.draw(m_name.GetTextBox());
	m_addProductFrame.draw(m_price.GetTextBox());
	m_addProductFrame.draw(m_retailer.GetTextBox());
	m_addProductFrame.draw(m_type.GetTextBox());
	m_addProductFrame.draw(m_addButton);
	m_addProductFrame.draw(m_addButtonText);
	m_addProductFrame.draw(m_name.GetText());
	m_addProductFrame.draw(m_price.GetText());
	m_addProductFrame.draw(m_retailer.GetText());
	m_addProductFrame.draw(m_type.GetText());
	m_addProductFrame.draw(m_nameLable);
	m_addProductFrame.draw(m_priceLable);
	m_addProductFrame.draw(m_retailerLable);
	m_addProductFrame.draw(m_typeLable);
}

const bool AddProductFrame::PressedTextBox(const sf::RectangleShape& textBox) const
{
	sf::FloatRect floatRect = textBox.getGlobalBounds();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		float x = static_cast<float>(sf::Mouse::getPosition(m_addProductFrame).x);
		float y = static_cast<float>(sf::Mouse::getPosition(m_addProductFrame).y);
		if (floatRect.contains(x, y))
		{
			return true;
		}
	}
	return false;
}
