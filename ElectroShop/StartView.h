#pragma once
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <vector>
#include "ProductImage.h"

class StartView
{
public:
	StartView();

	void ReadImages();
	void AddImages();
	void InitializeFont();
	
	sf::View GetView() const;
	int GetImagesNumber() const;
	const sf::RectangleShape& GetRectangleAt(const int index) const;
	const sf::Sprite& GetSpriteAt(const int index) const;
	const sf::Texture& GetImageAt(int index) const;
	const sf::Text& GetNameAt(int index) const;
	const sf::Text& GetPriceAt(int index) const;
	const sf::Text& GetRetailerAt(int index) const;

	void MoveElements(const std::string& direction);

private:
	sf::Font m_textFont;
	sf::View m_startView;
	std::vector<std::unique_ptr<ProductImage>> m_productImages;
	std::vector<std::unique_ptr<sf::RectangleShape>> m_productRectangles;
	std::vector<std::unique_ptr<sf::Sprite>> m_productSprite;
	std::vector<sf::Text> m_names;
	std::vector<sf::Text> m_prices;
	std::vector<sf::Text> m_retailers;
};

