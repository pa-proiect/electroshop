#include "User.h"

User::User(const std::string& firstName, const std::string& lastName, const std::string& email, const std::string& username, const std::string& password)
	:m_firstName(firstName), m_lastName(lastName), m_email(email), m_username(username), m_password(password)
{
}

const std::string& User::GetFirstName() const
{
	return m_firstName;
}

const std::string& User::GetLastName() const
{
	return m_lastName;
}

const std::string& User::GetEmail() const
{
	return m_email;
}

const std::string& User::GetUsername() const
{
	return m_username;
}

const std::string& User::GetPassword() const
{
	return m_password;
}
