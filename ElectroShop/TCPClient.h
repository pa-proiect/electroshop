#pragma once
#include <winsock2.h>
#include <ws2tcpip.h>
#include <cstdint>
#include <vector>
#include <string>

#pragma comment (lib, "Ws2_32.lib")

class TCPClient
{
public:
	TCPClient();

	void InitializeWinsock();
	void CreateClientSocket();
	void ConnectToServer();
	void SendLoginData(std::string& data);
	void SendRegisterData(std::string& data);
	void CloseSocket();
	void Run();

	const bool GetLogged() const;

private:
	WSADATA m_wsaData;  
	int m_initialize; 
	SOCKET m_connectSocket;
	sockaddr_in m_hint;
	bool m_logged;
};