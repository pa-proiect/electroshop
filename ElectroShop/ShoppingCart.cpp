#include "ShoppingCart.h"
#include <iostream>

ShoppingCart::ShoppingCart()
{
}

void ShoppingCart::AddProductImages()
{
	int coordXRect = -30;
	int coordYRect = -100;

	int coordXImage = -96;
	int coordYImage = -200;

	int index = 0;

	sf::RectangleShape productRect;
	sf::Sprite sprite;

	for (ProductImage productImage : m_productImages)
	{
		productRect.setOrigin(coordXRect, coordYRect);
		productRect.setSize(sf::Vector2f(300, 350));
		productRect.setFillColor(sf::Color(36, 33, 48));
		m_productRectangles.emplace_back(productRect);

		sprite.setScale(sf::Vector2f(0.57f, 0.57f));
		sprite.setOrigin(coordXImage, coordYImage);
		sprite.setTexture(m_productImages.at(index).GetImage());
		m_productSprite.emplace_back(sprite);

		coordXRect -= 320;
		coordXImage -= 557;

		if (index % 2 == 1)
		{
			coordXImage = -96;
			coordYImage -= 767;
			coordXRect = -30;
			coordYRect -= 437;
		}

		index++;
	}
}

void ShoppingCart::AddProduct(Product newProduct)
{
	m_products.push_front(newProduct);
}

void ShoppingCart::RemoveProduct(Product product)
{
	m_products.remove(product);
}

void ShoppingCart::MoveElements(const std::string& direction)
{
	if (direction == "up")
	{
		for (int index = 0; index < m_productImages.size(); index++)
		{
			m_productRectangles.at(index).move(m_productRectangles.at(index).getPosition().x, 20);
			m_productSprite.at(index).move(m_productSprite.at(index).getPosition().x, 20);
		}
	}
	if (direction == "down")
	{
		for (int index = 0; index < m_productImages.size(); index++)
		{
			m_productRectangles.at(index).move(m_productRectangles.at(index).getPosition().x, -20);
			m_productSprite.at(index).move(m_productSprite.at(index).getPosition().x, -20);
		}
	}
	if (direction != "up" && direction != "down")
		std::cerr << "Invalid direction" << std::endl;
}

const bool ShoppingCart::PressButton(sf::RectangleShape& button) const
{
	sf::FloatRect floatRect = button.getGlobalBounds();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		if (floatRect.contains(sf::Mouse::getPosition(m_cartFrame).x, sf::Mouse::getPosition(m_cartFrame).y))
		{
			return true;
		}
	}
	return false;
}

void ShoppingCart::CreateFrame()
{
	m_cartFrame.create(sf::VideoMode(600, 640), "Electro Shop");

	SetMenuBar();

	InitializeFonts();

	SetLogo();

	SetTotal();
	SetBuyButton();
}

void ShoppingCart::SetMenuBar()
{
	m_menuBar.setSize(sf::Vector2f(640, 70));
	m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void ShoppingCart::InitializeFonts()
{
	if (!m_logoFont.loadFromFile("LogoFont.ttf"))
		std::cerr << "LogoFont can't be loaded!" << std::endl;

	if (!m_buttonFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void ShoppingCart::SetLogo()
{
	m_logo.setString("ElectroShop");
	m_logo.setFont(m_logoFont);
	m_logo.setCharacterSize(45);
	m_logo.setFillColor(sf::Color(255, 206, 43));
	m_logo.setOrigin(sf::Vector2f(-190, 0));
}

void ShoppingCart::SetTotal()
{
	m_total.setString("Subtotal:");
	m_total.setCharacterSize(21);
	m_total.setFont(m_buttonFont);
	m_total.setFillColor(sf::Color::Black);
	m_total.setOrigin(sf::Vector2f(-440, -100));
}

void ShoppingCart::SetBuyButton()
{
	m_buyButton.setSize(sf::Vector2f(60, 30));
	m_buyButton.setFillColor(sf::Color(255, 206, 43));
	m_buyButton.setOrigin(sf::Vector2f(-480, -190));

	m_buyButtonText.setString("Buy");
	m_buyButtonText.setFont(m_buttonFont);
	m_buyButtonText.setCharacterSize(21);
	m_buyButtonText.setFillColor(sf::Color(36, 33, 48));
	m_buyButtonText.setOrigin(sf::Vector2f(-490, -190));
}

void ShoppingCart::RunFrame()
{
	CreateFrame();
	while (m_cartFrame.isOpen())
	{
		m_cartFrame.clear(sf::Color(255, 255, 255));

		sf::Event event;
		while (m_cartFrame.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_cartFrame.close();

			if (PressButton(m_buyButton))
			{
				m_cartFrame.close();
				m_registerFrame.RunFrame();
			}
		}

		DrawElements();
		m_cartFrame.display();
		m_cartFrame.setActive();
	}
}

void ShoppingCart::DrawElements()
{
	m_cartFrame.draw(m_menuBar);
	m_cartFrame.draw(m_logo);
	m_cartFrame.draw(m_buyButton);
	m_cartFrame.draw(m_buyButtonText);
	m_cartFrame.draw(m_total);
}