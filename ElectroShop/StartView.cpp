#include "StartView.h"
#include <iostream>
#include <fstream>

StartView::StartView()
{
	InitializeFont();
}

void StartView::ReadImages()
{
	std::ifstream fileProductNames("ProductNames.txt");
	std::ifstream fileProductData("ProductData.txt");
	std::vector<std::unique_ptr<std::string>> filenames;
	std::vector<std::string> names;
	std::vector<double> prices;
	std::vector<std::string> retailers;
	int numberOfProducts;
	fileProductNames >> numberOfProducts;
	for (int index = 0; index < numberOfProducts; index++)
	{
		std::unique_ptr<std::string> filename = std::make_unique<std::string>();
		fileProductNames >> *filename.get();

		filenames.emplace_back(std::move(filename));

		std::string name;
		double price;
		std::string retailer;
		fileProductData >> name >> price >> retailer;

		names.emplace_back(name);
		prices.emplace_back(price);
		retailers.emplace_back(retailer);
	}

	for (int index = 0; index < filenames.size(); index++)
	{
		std::unique_ptr<ProductImage> productImage = std::make_unique<ProductImage>(prices.at(index), retailers.at(index));
		std::unique_ptr<sf::Texture> image = std::make_unique<sf::Texture>();
		sf::Text nameText;
		sf::Text priceText;
		sf::Text retailerText;

		if (!image->loadFromFile(*filenames.at(index)))
		{
			std::cerr << "Can't load the image from file!";
		}

		productImage->SetImage(std::move(image));
		m_productImages.emplace_back(std::move(productImage));
		
		nameText.setString(names.at(index));
		m_names.emplace_back(nameText);
		
		priceText.setString(std::to_string(prices.at(index))+" RON");
		m_prices.emplace_back(priceText);
		
		retailerText.setString(retailers.at(index));
		m_retailers.emplace_back(retailerText);
	}

	fileProductNames.close();
	fileProductData.close();
}

void StartView::AddImages()
{
	int coordXRect = -30;
	int coordYRect = -100;

	int coordXImage = -96;
	int coordYImage = -200;
	
	int coordXName = -100;
	int coordYName = -380;

	int coordXPrice = -120;
	int coordYPrice = -410;

	for (int index = 0; index < m_productImages.size(); index++)
	{
		std::unique_ptr<sf::RectangleShape> productRect = std::make_unique<sf::RectangleShape>();
		std::unique_ptr<sf::Sprite> sprite = std::make_unique<sf::Sprite>();

		productRect->setOrigin(coordXRect, coordYRect);
		productRect->setSize(sf::Vector2f(300, 350));
		productRect->setFillColor(sf::Color(36, 33, 48));
		m_productRectangles.emplace_back(std::move(productRect));

		sprite->setScale(sf::Vector2f(0.57f, 0.57f));
		sprite->setOrigin(coordXImage, coordYImage);
		sprite->setTexture(m_productImages.at(index)->GetImage());
		m_productSprite.emplace_back(std::move(sprite));

		m_names.at(index).setOrigin(coordXName, coordYName);
		m_names.at(index).setFont(m_textFont);
		m_names.at(index).setFillColor(sf::Color(255,255,255));
		m_names.at(index).setCharacterSize(19);
		
		m_prices.at(index).setOrigin(coordXPrice, coordYPrice);
		m_prices.at(index).setFont(m_textFont);
		m_prices.at(index).setFillColor(sf::Color::White);
		m_prices.at(index).setCharacterSize(19);

		coordXRect -= 320;
		coordXImage -= 557;
		coordXName -= 320;
		coordXPrice -= 320;

		if (index % 3 == 2)
		{
			coordXImage = -96;
			coordYImage -= 767;
			coordXRect = -30;
			coordYRect -= 437;
			coordXName = -120;
			coordYName -= 440;
			coordXPrice = -120;
			coordYPrice -= 440;
		}
	}
}

void StartView::InitializeFont()
{
	if (!m_textFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "TextFont can't be loaded!" << std::endl;
}

sf::View StartView::GetView() const 
{
	return m_startView;
}

int StartView::GetImagesNumber() const
{
	return m_productImages.size();
}

const sf::RectangleShape& StartView::GetRectangleAt(const int index) const
{
	return *m_productRectangles.at(index).get();
}

const sf::Sprite& StartView::GetSpriteAt(const int index) const
{
	return *m_productSprite.at(index).get();
}

const sf::Texture& StartView::GetImageAt(int index) const
{
	return m_productImages.at(index)->GetImage();
}

const sf::Text& StartView::GetNameAt(int index) const
{
	return m_names.at(index);
}

const sf::Text& StartView::GetPriceAt(int index) const
{
	return m_prices.at(index);
}

const sf::Text& StartView::GetRetailerAt(int index) const
{
	return m_retailers.at(index);
}

void StartView::MoveElements(const std::string& direction)
{
	if (direction == "up")
	{
		for (int index = 0; index < m_productImages.size(); index++)
		{
			m_productRectangles.at(index)->move(m_productRectangles.at(index)->getPosition().x, 20);
			m_productSprite.at(index)->move(m_productSprite.at(index)->getPosition().x, 20);
			m_names.at(index).move(m_names.at(index).getPosition().x, 20);
			m_prices.at(index).move(m_names.at(index).getPosition().x, 20);
		}
	}
	if (direction == "down")
	{
		for (int index = 0; index < m_productImages.size(); index++)
		{
			m_productRectangles.at(index)->move(m_productRectangles.at(index)->getPosition().x, -20);
			m_productSprite.at(index)->move(m_productSprite.at(index)->getPosition().x, -20);
			m_names.at(index).move(m_names.at(index).getPosition().x, -20);
			m_prices.at(index).move(m_names.at(index).getPosition().x, -20);
		}
	}
	if (direction != "up" && direction != "down")
		std::cerr << "Invalid direction" << std::endl;
}
