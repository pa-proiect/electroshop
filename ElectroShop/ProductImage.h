#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <memory>

class ProductImage
{
public:
	ProductImage() = default;
	ProductImage(const double price, const std::string& sellerName);
	ProductImage(const ProductImage& productImage);
	ProductImage(ProductImage&& productImage);
	~ProductImage() = default;
	
	ProductImage& operator=(const ProductImage& productImage);
	ProductImage& operator=(ProductImage&& productImage);

	void SetImage(std::unique_ptr<sf::Texture> image);
	const sf::Texture& GetImage() const;
	const double GetPrice() const;
	const std::string& GetSellerName() const;

private:
	sf::Texture m_image;
	double m_price;
	std::string m_sellerName;
};

