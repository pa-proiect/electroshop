#pragma once
#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\System\String.hpp>
#include <SFML\Graphics\Text.hpp>
#include "InputType.h"

class InputTextBox
{
public:
    InputTextBox();

    void SetType(const std::string& type);
    void SetOrigin(const sf::Vector2f& size);
    void SetInput(const sf::String& input);
    void SetTextOrigin(const sf::Vector2f& origin);
    void SetTextFont(const sf::Font& font);

    const sf::RectangleShape& GetTextBox() const;
    const sf::Text& GetText() const;
    const InputType& GetInputType() const;

private:
    sf::RectangleShape m_textBox;
    sf::String m_input;
    sf::Text m_text;
    InputType m_type;
};

