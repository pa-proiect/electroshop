#include "Retailer.h"

Retailer::Retailer(const std::string& firstName, const std::string& lastName, const std::string& email, const std::string& username, const std::string& password)
	:User(firstName, lastName, email, username, password)
{
}

void Retailer::AddProduct(Product newProduct) 
{
	m_products.push_front(newProduct);
}

void Retailer::RemoveProduct(Product product) 
{
	m_products.remove(product);
}
