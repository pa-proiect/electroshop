#pragma once
#include <string>

enum class ProductBrand :int16_t
{
    Samsung = 10,
    LG,
    Apple,
    Asus,
    Acer,
    Sony,
    Lenovo,
    HP,
    Nikon,
    Canon,
    Huawei,
    Philips,
    Beats,
    Logitech,
    Nokia,
    Xiaomi,
};

inline ProductBrand ParseStringToProductBrand(const std::string& type)
{

    if (type == "Samsung")
    {
        return ProductBrand::Samsung;
    }

    if (type == "LG")
    {
        return ProductBrand::LG;
    }

    if (type == "Apple")
    {
        return ProductBrand::Apple;
    }

    if (type == "Asus")
    {
        return ProductBrand::Asus;
    }

    if (type == "Acer")
    {
        return ProductBrand::Acer;
    }

    if (type == "Sony")
    {
        return ProductBrand::Sony;
    }

    if (type == "Lenovo")
    {
        return ProductBrand::Lenovo;
    }

    if (type == "HP")
    {
        return ProductBrand::HP;
    }

    if (type == "Nikon")
    {
        return ProductBrand::Nikon;
    }

    if (type == "Canon")
    {
        return ProductBrand::Canon;
    }

    if (type == "Huawei")
    {
        return ProductBrand::Huawei;
    }

    if (type == "Philips")
    {
        return ProductBrand::Philips;
    }

    if (type == "Beats")
    {
        return ProductBrand::Beats;
    }

    if (type == "Logitech")
    {
        return ProductBrand::Logitech;
    }

    if (type == "Nokia")
    {
        return ProductBrand::Nokia;
    }

    if (type == "Xiaomi")
    {
        return ProductBrand::Xiaomi;
    }
}