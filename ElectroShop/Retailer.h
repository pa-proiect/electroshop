#pragma once
#include <forward_list>
#include "User.h"
#include "Product.h"

class Retailer : public User
{
public:
	Retailer(const std::string& firstName, const std::string& lastName, const std::string& email, const std::string& username, const std::string& password);

	void AddProduct(Product newProduct);
	void RemoveProduct(Product product);

private:
	std::forward_list<Product> m_products;
	
};

