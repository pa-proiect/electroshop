#pragma once
#include <SFML/Graphics.hpp>
#include "TCPClient.h"
#include"ProductImage.h"
#include "StartView.h"
#include "LoginFrame.h"
#include "RegisterFrame.h"
#include "ProductFrame.h"
#include "ShoppingCart.h"
#include "WishList.h"
#include "AddProductFrame.h"

class MainFrame
{
public:
	MainFrame(const std::shared_ptr<TCPClient>& tcp);

	void SetMenuBar();
	void InitializeFonts();
	void SetLogo();
	void SetCartButton();
	void SetWishButton();
	void SetAddProductButton();
	void SetLoginButton();
	void SetRegisterButton();	
	void CreateFrame();
	void RunFrame();
	void DrawElements();

	const bool PressButton(const sf::RectangleShape& button) const;
	const bool PressButton(const sf::Sprite& button) const;
	
private:
	std::shared_ptr<TCPClient> m_tcp;
	sf::RenderWindow m_mainFrame;
	sf::RectangleShape m_menuBar;
	sf::Font m_logoFont;
	sf::Text m_logo;
	sf::RectangleShape m_login;
	sf::Text m_loginText;
	sf::RectangleShape m_register;
	sf::Text m_registerText;
	sf::Font m_buttonsFont;
	StartView m_startView;
    LoginFrame m_loginFrame;
	RegisterFrame m_registerFrame;
	ProductFrame m_productFrame;
	sf::Texture m_cartImage;
	sf::Sprite m_cartButton;
	sf::Texture m_wishImage;
	sf::Sprite m_wishButton;
	sf::Texture m_addProductImage;
	sf::Sprite m_addProductButton;
	ShoppingCart m_cartFrame;
	WishList m_wishFrame;
	AddProductFrame m_addProductFrame;
};

