#pragma once
#include "User.h"
#include "ShoppingCart.h"
#include "WishList.h"

class Buyer : public User
{
public:
	Buyer(const std::string& firstName, const std::string& lastName, const std::string& email, const std::string& username, const std::string& password);

private:
	ShoppingCart m_shoppingCart;
	WishList m_wishlist;
};

