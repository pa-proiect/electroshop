#include "ProductFrame.h"
#include "ShoppingCart.h"
#include <SFML/Graphics.hpp>
#include <iostream>

ProductFrame::ProductFrame(Product product, ProductImage productImage)
    :m_product(product), m_productImage(productImage)
{
    m_cartPress = false;
    m_wishPress = false;
}

ProductImage ProductFrame::GetProductImage() const
{
    return m_productImage;
}

Product ProductFrame::GetProduct() const
{
    return m_product;
}

const bool ProductFrame::GetIfPressCart() const
{
    return m_cartPress;
}

const bool ProductFrame::GetIfPressWish() const
{
    return m_wishPress;
}

void ProductFrame::AddImage()
{

}

void ProductFrame::SetCartButton()
{
    m_cartButton.setSize(sf::Vector2f(120, 40));
    m_cartButton.setFillColor(sf::Color(255, 206, 43));
    m_cartButton.setOrigin(sf::Vector2f(-140, -600));

    m_cartButtonText.setString("Add to Cart");
    m_cartButtonText.setFont(m_buttonFont);
    m_cartButtonText.setCharacterSize(17);
    m_cartButtonText.setFillColor(sf::Color(36, 33, 48));
    m_cartButtonText.setOrigin(sf::Vector2f(-145, -605));
}

void ProductFrame::SetWishButton()
{
    m_wishButton.setSize(sf::Vector2f(180, 40));
    m_wishButton.setFillColor(sf::Color(255, 206, 43));
    m_wishButton.setOrigin(sf::Vector2f(-320, -600));

    m_wishButtonText.setString("Add to WishList");
    m_wishButtonText.setFont(m_buttonFont);
    m_wishButtonText.setCharacterSize(17);
    m_wishButtonText.setFillColor(sf::Color(36, 33, 48));
    m_wishButtonText.setOrigin(sf::Vector2f(-330, -605));
}

void ProductFrame::SetRectangle()
{
    m_rectangle.setOrigin(50, 50);
    m_rectangle.setSize(sf::Vector2f(50, 50));
    m_rectangle.setFillColor(sf::Color(36, 33, 48));
}

void ProductFrame::SetImage(const sf::Texture& texture)
{
    m_image.setScale(sf::Vector2f(0.7f, 0.7f));
    m_image.setOrigin(-230, -150);
    m_image.setTexture(texture);
}

void ProductFrame::SetName(const std::string& name)
{
    m_prodName.setString(name);
    m_prodName.setOrigin(-240, -450);
    m_prodName.setFont(m_buttonFont);
    m_prodName.setFillColor(sf::Color::Black);
    m_prodName.setCharacterSize(25);
}

void ProductFrame::SetPrice(const std::string& price)
{
    m_prodPrice.setString(price);
    m_prodPrice.setOrigin(-280, -490);
    m_prodPrice.setFont(m_buttonFont);
    m_prodPrice.setFillColor(sf::Color::Black);
    m_prodPrice.setCharacterSize(25);
}

void ProductFrame::SetRetailer(const std::string& retailer)
{
    m_prodRetailer.setString(retailer);
    m_prodRetailer.setOrigin(-290, -550);
    m_prodRetailer.setFont(m_buttonFont);
    m_prodRetailer.setFillColor(sf::Color::Black);
    m_prodRetailer.setCharacterSize(22);
}

void ProductFrame::SetMenuBar()
{
    m_menuBar.setSize(sf::Vector2f(640, 70));
    m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void ProductFrame::InitializeFonts()
{
    if (!m_logoFont.loadFromFile("LogoFont.ttf"))
        std::cerr << "LogoFont can't be loaded!" << std::endl;

    if (!m_buttonFont.loadFromFile("ButtonsFont.ttf"))
        std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void ProductFrame::SetLogo()
{
    m_logo.setString("ElectroShop");
    m_logo.setFont(m_logoFont);
    m_logo.setCharacterSize(45);
    m_logo.setFillColor(sf::Color(255, 206, 43));
    m_logo.setOrigin(sf::Vector2f(-190, 0));
}

void ProductFrame::CreateFrame()
{
    m_productFrame.create(sf::VideoMode(640, 720), "Electro Shop");

    SetMenuBar();

    InitializeFonts();

    SetLogo();

    SetCartButton();
    SetWishButton();
    SetRectangle();
}

void ProductFrame::RunFrame()
{
    CreateFrame();

    while (m_productFrame.isOpen())
    {
        m_productFrame.clear(sf::Color(255, 255, 255));

        sf::Event event;
        while (m_productFrame.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                m_productFrame.close();
            if (PressButton(m_cartButton))
            {
                m_productFrame.close();
                m_cartPress = true;  
            }
            if (PressButton(m_wishButton))
            {
                m_productFrame.close();
                m_wishPress = true;
            }
        }

        DrawElements();

        m_productFrame.display();
        m_productFrame.setActive();
    }
}

void ProductFrame::DrawElements()
{
    m_productFrame.draw(m_menuBar);
    m_productFrame.draw(m_logo);
    m_productFrame.draw(m_cartButton);
    m_productFrame.draw(m_cartButtonText);
    m_productFrame.draw(m_wishButton);
    m_productFrame.draw(m_wishButtonText);
    m_productFrame.draw(m_rectangle);
    m_productFrame.draw(m_image);
    m_productFrame.draw(m_prodName);
    m_productFrame.draw(m_prodPrice);
    m_productFrame.draw(m_prodRetailer);
}

const bool ProductFrame::PressButton(sf::RectangleShape& button) const
{
    sf::FloatRect floatRect = button.getGlobalBounds();

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        if (floatRect.contains(sf::Mouse::getPosition(m_productFrame).x, sf::Mouse::getPosition(m_productFrame).y))
        {
            return true;
        }
    }
    return false;
}

