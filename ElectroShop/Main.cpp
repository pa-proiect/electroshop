#include <thread>
#include "MainFrame.h"
#include "TCPClient.h"

int main()
{
	std::shared_ptr<TCPClient> tcp = std::make_shared<TCPClient>();
	std::thread tcpThread(&TCPClient::Run, &*tcp);
	
	MainFrame mainFrame(tcp);
	std::thread mainFrameThread(&MainFrame::RunFrame, &mainFrame);

	tcpThread.join();
	mainFrameThread.join();

	return 0;
}