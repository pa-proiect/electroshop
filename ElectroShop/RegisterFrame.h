#pragma once
#include <SFML/Graphics.hpp>
#include "InputType.h"
#include "InputTextBox.h"
#include "LoginFrame.h"
#include "TCPClient.h"
#include <regex>

class RegisterFrame
{
public:
	RegisterFrame();

	void SetTCP(const std::shared_ptr<TCPClient>& tcp);

	void SetMenuBar();
	void InitializeFonts();
	void SetLogo();
	void SetLables();
	void SetInput(const std::string& type, sf::Vector2f origin);
	void SetRegisterButton();
	void SetLoginButton();
	void SetInvalidPassword();
	void SetInvalidMailText();
	void SetInvalidUsernameText();
	void SetInvalidRepassword();
	
	void CreateFrame();                                  
	void RunFrame();
	void DrawElements();    

	const bool CheckPassword(const sf::Text& passwordText, bool& lengthOrUpperCase) const;
	const bool CheckEmail(const sf::Text& emailText) const;
	const bool CheckUsername(const sf::Text& usernameText) const;
	const bool CheckRepassword(const sf::Text& passwordText, const sf::Text& repasswordText) const;

	const sf::RectangleShape& GetLoginButton() const;

	const bool PressButton(const sf::RectangleShape& button) const;
	const bool PressedTextBox(const sf::RectangleShape& textBox) const;

private:
	std::shared_ptr<TCPClient> m_tcp;
	sf::RenderWindow m_registerFrame;
	sf::RectangleShape m_menuBar;
	sf::Font m_logoFont;
	sf::Text m_logo;
	InputTextBox m_firstName;
	InputTextBox m_lastName;
	InputTextBox m_address;
	InputTextBox m_email;
	InputTextBox m_username;
	InputTextBox m_password;
	InputTextBox m_repassword;
	sf::Text m_firstNameLable;
	sf::Text m_lastNameLable;
	sf::Text m_addressLable;
	sf::Text m_mailLable;
	sf::Text m_usernameLable;
	sf::Text m_passwordLable;
	sf::Text m_repasswordLable;
	sf::Font m_buttonFont;
	sf::RectangleShape m_registerButton;
	sf::Text m_registerText;
	sf::RectangleShape m_loginButton;
	sf::Text m_loginText;
	sf::Text m_haveAccount;
	LoginFrame m_loginFrame;
	sf::Text m_textInvalidPassword;
	sf::Text m_textInvalidMail;
	sf::Text m_textInvalidUsername;
	sf::Text m_invalidRepassword;
};

