#pragma once
#include <forward_list>
#include "Product.h"
#include "ProductImage.h"
#include "ShoppingCart.h"
#include <SFML/Graphics.hpp>

class WishList
{
public:
	WishList();

	void AddProductImages();
	void AddProduct(Product newProduct);
	void RemoveProduct(Product product);
	void AddToShoppingCart(Product product);
	void MoveElements(const std::string& direction);

	void CreateFrame();
	void SetMenuBar();
	void InitializeFonts();
	void SetLogo();
	void SetBuyButton();
	void RunFrame();
	void DrawElements();

private:
	std::forward_list<Product> m_wishProducts;
	sf::RenderWindow m_wishFrame;
	sf::RectangleShape m_menuBar;
	sf::Font m_logoFont;
	sf::Text m_logo;
	sf::Font m_buttonFont;
	std::vector<ProductImage> m_productImages;
	std::vector<sf::RectangleShape> m_productRectangles;
	std::vector<sf::Sprite> m_productSprite;
	sf::RectangleShape m_buyButton;
	sf::Text m_buyButtonText;
	sf::RectangleShape m_removeButton;
	sf::Text m_removeText;
	ShoppingCart m_products;
};