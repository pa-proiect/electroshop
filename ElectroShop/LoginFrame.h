#pragma once
#include <SFML/Graphics.hpp>
#include "InputTextBox.h"
#include "TCPClient.h"

class LoginFrame
{
public:
	LoginFrame();

	void SetTCP(const std::shared_ptr<TCPClient>& tcp);

	void SetMenuBar();
	void InitializeFonts();
	void SetLogo();
	void SetLables();
	void SetInput(const std::string& type, sf::Vector2f origin);
	void SetLoginButton();
	void CreateFrame();
	void RunFrame();
	void DrawElements();

	const bool PressedTextBox(const sf::RectangleShape& textBox) const;

private:
	std::shared_ptr<TCPClient> m_tcp;
	sf::RenderWindow m_loginFrame;
	sf::RectangleShape m_menuBar;
	sf::Font m_logoFont;
	sf::Text m_logo;
	InputTextBox m_mail;
	InputTextBox m_password;
	sf::Text m_mailLable;
	sf::Text m_passwordLable;
	sf::Font m_buttonFont;
	sf::RectangleShape m_loginButton;
	sf::Text m_loginText;
};

