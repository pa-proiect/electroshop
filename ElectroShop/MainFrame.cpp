#include "MainFrame.h"
#include <iostream>
#include <fstream>

MainFrame::MainFrame(const std::shared_ptr<TCPClient>& tcp)
	:m_tcp(tcp)
{
	m_loginFrame.SetTCP(tcp);
	m_registerFrame.SetTCP(tcp);
}

void MainFrame::SetMenuBar()
{
	m_menuBar.setSize(sf::Vector2f(1080, 70));
	m_menuBar.setFillColor(sf::Color(36, 33, 48));
}

void MainFrame::InitializeFonts()
{
	if (!m_logoFont.loadFromFile("LogoFont.ttf"))
		std::cerr << "LogoFont can't be loaded!" << std::endl;
	
	if (!m_buttonsFont.loadFromFile("ButtonsFont.ttf"))
		std::cerr << "ButtonsFont can't be loaded!" << std::endl;
}

void MainFrame::SetLogo()
{
    m_logo.setString("ElectroShop");
	m_logo.setFont(m_logoFont);
	m_logo.setCharacterSize(45);
	m_logo.setFillColor(sf::Color(255, 206, 43));
	m_logo.setOrigin(sf::Vector2f(-30, 0));
}

void MainFrame::SetCartButton()
{
	m_cartImage.loadFromFile("shoppingCart.png");
	m_cartButton.setTexture(m_cartImage);
	m_cartButton.setScale(sf::Vector2f(0.05f, 0.05f));
	m_cartButton.setOrigin(sf::Vector2f(-9000,-500));
}

void MainFrame::SetWishButton()
{
	m_wishImage.loadFromFile("heart.png");
	m_wishButton.setTexture(m_wishImage);
	m_wishButton.setScale(sf::Vector2f(0.03f, 0.03f));
	m_wishButton.setOrigin(sf::Vector2f(-17400, -1000));
}

void MainFrame::SetAddProductButton()
{
	m_addProductImage.loadFromFile("plus1.png");
	m_addProductButton.setTexture(m_addProductImage);
	m_addProductButton.setScale(sf::Vector2f(0.09f, 0.09f));
	m_addProductButton.setOrigin(sf::Vector2f(-6500, -300));
}

void MainFrame::SetLoginButton()
{
	m_login.setSize(sf::Vector2f(90, 40));
	m_login.setFillColor(sf::Color(255, 206, 43));
	m_login.setOrigin(sf::Vector2f(-800, -15));

	m_loginText.setString("Log In");
	m_loginText.setFont(m_buttonsFont);
	m_loginText.setCharacterSize(17);
	m_loginText.setFillColor(sf::Color(36, 33, 48));
	m_loginText.setOrigin(sf::Vector2f(-813, -25));
}

void MainFrame::SetRegisterButton()
{
	m_register.setSize(sf::Vector2f(90, 40));
	m_register.setFillColor(sf::Color(255, 206, 43));
	m_register.setOrigin(sf::Vector2f(-910, -15));

	m_registerText.setString("Register");
	m_registerText.setFont(m_buttonsFont);
	m_registerText.setCharacterSize(17);
	m_registerText.setFillColor(sf::Color(36, 33, 48));
	m_registerText.setOrigin(sf::Vector2f(-915, -25));
}

void MainFrame::CreateFrame()
{
	m_mainFrame.create(sf::VideoMode(1080, 640), "Electro Shop");

	InitializeFonts();

	SetMenuBar();
	SetLogo();
	SetCartButton();
	SetWishButton();
	SetAddProductButton();
	SetLoginButton();
	SetRegisterButton();

	m_startView.GetView().reset(sf::FloatRect(0, 70, 1080, 570));
	m_startView.ReadImages();
	m_startView.AddImages();
}

void MainFrame::RunFrame()
{
	CreateFrame();

	while (m_mainFrame.isOpen())
	{
		m_mainFrame.clear(sf::Color(255, 255, 255));

		sf::Event event;
		while (m_mainFrame.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_mainFrame.close();

			if (event.type == sf::Event::MouseWheelMoved)
			{
				if (event.mouseWheel.delta > 0)
				{
					m_startView.MoveElements("up");
				}
				if (event.mouseWheel.delta < 0)
				{
					m_startView.MoveElements("down");
				}
			}

			if (PressButton(m_login))
			{
				m_loginFrame.RunFrame();
			}

			if (PressButton(m_register))
			{
				m_registerFrame.RunFrame();
				if (PressButton(m_registerFrame.GetLoginButton()))
				{
					m_loginFrame.RunFrame();
				}
			}
			if (PressButton(m_cartButton))
			{
				m_cartFrame.RunFrame();
					
				if (event.type == sf::Event::MouseWheelMoved)
				{
					if (event.mouseWheel.delta > 0)
					{
						m_cartFrame.MoveElements("up");
					}
					if (event.mouseWheel.delta < 0)
					{
						m_cartFrame.MoveElements("down");
					}
				}
			}
			if (PressButton(m_wishButton))
			{
				m_wishFrame.RunFrame();
				if (event.type == sf::Event::MouseWheelMoved)
				{
					if (event.mouseWheel.delta > 0)
					{
						m_wishFrame.MoveElements("up");
					}
					if (event.mouseWheel.delta < 0)
					{
						m_wishFrame.MoveElements("down");
					}
				}
			}
			if (PressButton(m_addProductButton))
			{
				m_addProductFrame.RunFrame();
			}
			for (int index = 0; index < m_startView.GetImagesNumber(); index++)
			{
				sf::RectangleShape prod;
				prod = m_startView.GetRectangleAt(index);

				sf::Text prodName = m_startView.GetNameAt(index);
				sf::Text prodPrice = m_startView.GetPriceAt(index);
				sf::Text prodRetailer = m_startView.GetRetailerAt(index);

				if (PressButton(prod))
				{	
					sf::Texture texture = m_startView.GetImageAt(index);
					m_productFrame.SetImage(texture);
					m_productFrame.SetName(prodName.getString());
					m_productFrame.SetPrice(prodPrice.getString());
					m_productFrame.SetRetailer(prodRetailer.getString());
					m_productFrame.RunFrame();
				}
			}
		}

		DrawElements();

		m_mainFrame.setActive();

		m_mainFrame.display();
	}
}

void MainFrame::DrawElements()
{
	for (int index = 0; index < m_startView.GetImagesNumber(); index++)
	{
		m_mainFrame.draw(m_startView.GetRectangleAt(index));
		m_mainFrame.draw(m_startView.GetSpriteAt(index));
		
		m_mainFrame.draw(m_startView.GetNameAt(index));
		m_mainFrame.draw(m_startView.GetPriceAt(index));
		m_mainFrame.draw(m_startView.GetRetailerAt(index));
	}

	m_mainFrame.draw(m_menuBar);
	m_mainFrame.draw(m_logo);
	m_mainFrame.draw(m_login);
	m_mainFrame.draw(m_loginText);
	m_mainFrame.draw(m_cartButton);
	m_mainFrame.draw(m_wishButton);
	m_mainFrame.draw(m_register);
	m_mainFrame.draw(m_registerText);
	m_mainFrame.draw(m_addProductButton);
}

const bool MainFrame::PressButton(const sf::RectangleShape& button) const
{
	sf::FloatRect floatRect = button.getGlobalBounds();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		float x = static_cast<float>(sf::Mouse::getPosition(m_mainFrame).x);
		float y = static_cast<float>(sf::Mouse::getPosition(m_mainFrame).y);
		if (floatRect.contains(x, y)) 
		{
			return true;
		}
	}
	return false;
}

const bool MainFrame::PressButton(const sf::Sprite& button) const
{
	sf::FloatRect rect = button.getGlobalBounds();
	bool isButtonPressed = static_cast<bool>(sf::Mouse::isButtonPressed(sf::Mouse::Left));
	if (isButtonPressed)
	{
		float x = static_cast<float>(sf::Mouse::getPosition(m_mainFrame).x);
		float y = static_cast<float>(sf::Mouse::getPosition(m_mainFrame).y);
		bool rectContains = static_cast<bool>(rect.contains(x, y));
		if (rectContains)
		{
			return true;
		}
	}
	return false;
}
