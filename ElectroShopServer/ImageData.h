#pragma once
#include <fstream>
#include <vector>
#include <SFML/Graphics/Texture.hpp>

class ImageData
{
public:
	ImageData() = default;

	void ReadFilenames();
	const sf::Texture& LoadTexutre(int index) const;

	const int GetNumberOfFiles() const;

private:
	std::ifstream m_fileProductNames;
	std::vector<std::unique_ptr<std::string>> m_filenames;
};

