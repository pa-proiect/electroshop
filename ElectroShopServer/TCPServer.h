#pragma once
#include "ImageData.h"
#include <winsock2.h>
#include <WS2tcpip.h>
#include <cstdint>
#include <fstream>

#pragma comment(lib, "Ws2_32.lib")

class TCPServer
{
public:
	TCPServer();
	~TCPServer() = default;

	void InitializeWinsock();
	void CreateServerSocket();
	void BindListenSocket();
	void WaitConnection();
	void ReceiveProcessUserData();
	void SendStartViewData();

private:
	WSADATA m_wsadata;
	int m_initialize;
	SOCKET m_listeningSocket;
	const uint16_t m_PORT = 54000;
	sockaddr_in m_hint;
	sockaddr_in m_client;
	SOCKET m_clientSocket;
	ImageData m_image;
	std::vector<std::string> m_usersRegisteredData;
	bool m_logged;
	std::ifstream m_usersDataLoginInput;
	std::ofstream m_usersDataLoginOutput;
	std::ofstream m_usersDataOutput;
};
