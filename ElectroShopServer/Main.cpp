#include <iostream>
#include "TCPServer.h"

int main()
{
	TCPServer ElectroShopServer;
	ElectroShopServer.InitializeWinsock();
	ElectroShopServer.CreateServerSocket();
	ElectroShopServer.BindListenSocket();
	ElectroShopServer.WaitConnection();
	ElectroShopServer.ReceiveProcessUserData();

	return 0;
}