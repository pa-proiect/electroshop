#include "ImageData.h"
#include <iostream>

void ImageData::ReadFilenames()
{
	m_fileProductNames.open("ProductNames.txt");
	int numberOfProducts;
	m_fileProductNames >> numberOfProducts;

	for (int index = 0; index < numberOfProducts; index++)
	{
		std::unique_ptr<std::string> filename = std::make_unique<std::string>();
		m_fileProductNames >> *filename;

		m_filenames.emplace_back(std::move(filename));
	}
	m_fileProductNames.close();
}

const sf::Texture& ImageData::LoadTexutre(int index) const
{
	std::unique_ptr<sf::Texture> image = std::make_unique<sf::Texture>();
	if (!image->loadFromFile(*m_filenames.at(index)))
	{
		std::cerr << "Can't load image form file" << std::endl;
	}

	return std::move(*image);
}

const int ImageData::GetNumberOfFiles() const
{
	return m_filenames.size();
}
