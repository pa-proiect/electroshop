#include "TCPServer.h"
#include <iostream>
#include <memory>
#include <thread>

TCPServer::TCPServer()
{
	m_usersDataLoginInput.open("UserLoginData.txt");
	m_usersDataLoginOutput.open("UserLoginData.txt");
	m_usersDataOutput.open("UserData.txt");

	std::string userData;
	while (m_usersDataLoginInput >> userData)
	{
		m_usersRegisteredData.emplace_back(userData);
	}
}

void TCPServer::InitializeWinsock()
{
	m_initialize = WSAStartup(MAKEWORD(2, 2), &m_wsadata);
	if (m_initialize != 0)
	{
		std::cerr << "WSASturtup failed";
	}
}

void TCPServer::CreateServerSocket()
{
	m_listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_listeningSocket == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket";
	}
}

void TCPServer::BindListenSocket()
{
	m_hint.sin_family = AF_INET;
	m_hint.sin_port = htons(m_PORT);
	m_hint.sin_addr.S_un.S_addr = INADDR_ANY;

	bind(m_listeningSocket, (sockaddr*)&m_hint, sizeof(m_hint));

	listen(m_listeningSocket, SOMAXCONN);
}

void TCPServer::WaitConnection()
{
	int clientSize = sizeof(m_client);

	m_clientSocket = accept(m_listeningSocket, (sockaddr*)&m_client, &clientSize);

	char host[NI_MAXHOST];
	char service[NI_MAXSERV];

	ZeroMemory(host, NI_MAXHOST);
	ZeroMemory(service, NI_MAXSERV);

	if (getnameinfo((sockaddr*)&m_client, sizeof(m_client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		std::cout << host << " connected on port " << service << std::endl;
	}
	else
	{
		inet_ntop(AF_INET, &m_client.sin_addr, host, NI_MAXHOST);
		std::cout << host << " connected on port " << ntohs(m_client.sin_port) << std::endl;
	}

	closesocket(m_listeningSocket);
}

void TCPServer::ReceiveProcessUserData()
{
	char buf[4096];

	while (true)
	{
		ZeroMemory(buf, 4096);
		int bytesReceived = recv(m_clientSocket, buf, 4096, 0);
		
		if (bytesReceived == SOCKET_ERROR)
		{
			std::cerr << "Error in recv!. Quitting!" << std::endl;
			break;
		}
		if (bytesReceived == 0)
		{
			std::cout << "Client disconnected!" << std::endl;
			break;
		} 

		if (std::string(buf, 0, bytesReceived).at(0) != '~')
		{
			m_usersDataLoginOutput << std::string(buf, 0, bytesReceived) << std::endl;
			m_usersDataOutput << std::string(buf, 0, bytesReceived) << std::endl;
		}
		else
		{
			if (std::find(m_usersRegisteredData.begin(), m_usersRegisteredData.end(), std::string(buf, 0, bytesReceived)) != m_usersRegisteredData.end())
			{
				send(m_clientSocket, std::string("true").c_str(), 4, 0);
			}
			else
			{
				send(m_clientSocket, std::string("false").c_str(), 5, 0);
			}
		}
	}

	closesocket(m_clientSocket);

	WSACleanup();
}

void TCPServer::SendStartViewData()
{
	char buf[4096];
	m_image.ReadFilenames();

	int index = 0;

	while (true)
	{
		ZeroMemory(buf, 4096);

		int bytesReceived = recv(m_clientSocket, buf, 4096, 0);
		if (bytesReceived == SOCKET_ERROR)
		{
			std::cerr << "Error in recv! Quitting!" << std::endl;
			break;
		}
		if (bytesReceived == 0)
		{
			std::cout << "Client disconected!" << std::endl;
			break;
		}

		if (index < m_image.GetNumberOfFiles())
		{
			std::unique_ptr<sf::Texture> newTexture = std::make_unique<sf::Texture>(m_image.LoadTexutre(index));
			memcpy(&buf, &newTexture, sizeof(newTexture));
			send(m_clientSocket, buf, sizeof(buf), 0);
		}
		index++;
	}
}