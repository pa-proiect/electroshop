#include "gtest/gtest.h"
#include "../ElectroShopServer/TCPServer.h"

TEST(TCPServerTest, WinSockInitialization) {
	TCPServer tcp;
	EXPECT_NO_THROW(tcp.InitializeWinsock());
	EXPECT_NO_FATAL_FAILURE(tcp.InitializeWinsock());
}


TEST(TCPServerTest, CreateServerSocket) {
	TCPServer tcp;
	EXPECT_NO_THROW(tcp.CreateServerSocket());
	EXPECT_NO_FATAL_FAILURE(tcp.CreateServerSocket());
}

TEST(TCPServerTest, BindListenSocket) {
	TCPServer tcp;
	EXPECT_NO_THROW(tcp.BindListenSocket());
	EXPECT_NO_FATAL_FAILURE(tcp.BindListenSocket());
}

TEST(TCPServerTest, WaitConnection) {
	TCPServer tcp;
	EXPECT_NO_THROW(tcp.BindListenSocket());
	EXPECT_NO_FATAL_FAILURE(tcp.BindListenSocket());
}

TEST(TCPServerTest, ReceiveProcessUserData) {
	TCPServer tcp;
	EXPECT_NO_THROW(tcp.ReceiveProcessUserData());
	EXPECT_NO_FATAL_FAILURE(tcp.ReceiveProcessUserData());
}