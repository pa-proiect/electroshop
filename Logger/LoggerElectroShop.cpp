#include "ElectroShopLogger.h"
#include "pch.h"
#include "LoggerElectroShop.h"

LoggerES::LoggerElectroShop::LoggerElectroShop()
{
}

LoggerES::LoggerElectroShop::LoggerElectroShop(const std::string& filename)
{
}

LoggerES::LoggerElectroShop::~LoggerElectroShop()
{
}

void LoggerES::LoggerElectroShop::SetDateFormat(std::string dateFormat)
{
}

void LoggerES::LoggerElectroShop::WriteFileString(m_logLevel level, const std::string& text) const
{
}

void LoggerES::LoggerElectroShop::WriteFileLog(m_logLevel level, const std::string& text) const
{
}

void LoggerES::LoggerElectroShop::WriteConsoleLog(m_logLevel level, const std::string& text) const
{
}

void LoggerES::LoggerElectroShop::WriteFile(m_logLevel level, const std::string& text) const
{
}
