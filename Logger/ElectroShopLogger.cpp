#include "pch.h"
#include "ElectroShopLogger.h"

LoggerES::LoggerElectroShop::LoggerElectroShop()
{
}

LoggerES::LoggerElectroShop::LoggerElectroShop(const std::string& filename)
{
	try
	{
		if (!m_logFile.is_open())
		{
			m_logFile.open(filename, std::ofstream::out | std::ofstream::app);
			m_dateFormat = { "[%Y-%m-%d %H-:%M-:%S]" };
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception " << e.what();
	}
}

LoggerES::LoggerElectroShop::~LoggerElectroShop()
{
	m_logFile.close();
}

void LoggerES::LoggerElectroShop::SetDateFormat(std::string dateFormat)
{
	m_dateFormat = dateFormat;
}

void LoggerES::LoggerElectroShop::WriteFileString(m_logLevel level, const std::string& text)
{
	WriteLog(level, text, true);
}

void LoggerES::LoggerElectroShop::WriteConsoleString(m_logLevel level, const std::string& text)
{
	WriteLog(level, text, false);
}

void LoggerES::LoggerElectroShop::WriteLog(m_logLevel level, const std::string& text, bool isFile)
{
	try
	{
		m_semaphore.lock();
		if (isFile)
		{
			m_logFile << GetTimeStamp() << " " << GetLogLevelString() << text << std::endl;
		}
		else
		{
			std::cout << GetTimeStamp() << " " << GetLogLevelString() << text << std::endl;
		}
		m_semaphore.unlock();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception " << e.what();
	}
}

const std::string& LoggerES::LoggerElectroShop::GetTimeStamp() const
{
	char buff[100];
	time_t t = time(nullptr);
	tm tm;
	localtime_s(&tm, &t);
	strftime(buff, sizeof(buff), m_dateFormat.c_str(), &tm);
	return std::string(buff);
}

const std::string& LoggerES::LoggerElectroShop::GetLogLevelString(int level) const
{
	std::string levelString;
	switch (level) 
	{
	case 0: 
		levelString = "WARN: "; 
		break;
	case 1: 
		levelString = "ERROR: "; 
		break;
	case 2: 
		levelString = "INFO: "; 
		break;
	}
	return levelString;
}
