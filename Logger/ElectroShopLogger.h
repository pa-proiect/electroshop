#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <mutex>
#include <vector>

namespace LoggerES
{
	class LoggerElectroShop
	{
	public:
		LoggerElectroShop();
		LoggerElectroShop(const std::string& filename);
		~LoggerElectroShop();

		enum m_logLevel { LOGWARN, LOGERROR, LOGINFO };

		void SetDateFormat(std::string dateFormat);

		void WriteFileString(m_logLevel level, const std::string& text);
		void WriteConsoleString(m_logLevel level, const std::string& text);
		void WriteLog(m_logLevel level, const std::string& text, bool isFile);

		const std::string& GetTimeStamp() const;
		const std::string& GetLogLevelString(int level) const;

	private:
		std::ofstream m_logFile;
		std::string m_dateFormat;
		std::mutex m_semaphore;
		const std::vector<std::string> m_logLevelText;
	};
}


